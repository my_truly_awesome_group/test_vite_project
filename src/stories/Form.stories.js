
import Form from '../Form';

export default {
  title: 'Example/Form',
  component: Form,
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
    layout: 'fullscreen',
  },
};

export const FormDefault = {};
